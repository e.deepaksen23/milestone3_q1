package com.example.demo.milestone3_q3_initialiser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Milestone3Q3InitialiserApplication {

	public static void main(String[] args) {
		SpringApplication.run(Milestone3Q3InitialiserApplication.class, args);
	}

}
