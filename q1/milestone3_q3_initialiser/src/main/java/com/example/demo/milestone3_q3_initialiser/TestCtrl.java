/* this is an example of creating spring boot application thorugh initializr
 * spring parent, spring web, devtools, jasper and maven are added and a zip file is downloaded.
 * this folder is the imported as project into eclipse.
 * this example asks for first name and last name and concatenates it.
 * it then takes you to another page
 * */



package com.example.demo.milestone3_q3_initialiser;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class TestCtrl {
	@RequestMapping("/sen")
public String display1() {return "/homePage";}
	@RequestMapping("/cat")
	public String display2(HttpServletRequest req,HttpServletResponse res,Model model) {
		String s1=req.getParameter("fname");
		String s2=req.getParameter("lname");
		String s3=s1+" "+s2;
		model.addAttribute("fname",s1);
		model.addAttribute("lname",s2);
		model.addAttribute("fullname",s3);
		return "/resultPage";}
	
}
