/* this is an example of adding spring manually through dependencies
 * spring parent spring web and maven build paths are configuered in pom.xml
 * this example turns the colour of page to whatever colour given 
 * */


package com.godigit.springWebManual;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class TestController {
@RequestMapping("/red")
@ResponseBody
public String printRed() {
	return "<html>"
			+ "   <head>"
			+ "      <style>"
			+ "         #color {"
			+ "            width: 1000px;"
			+ "            height: 470px;"
			+ "            background: red;"
			+ "         }"
			+ "      </style>"
			+ "   <head>"
			+ "   <body>"
			+ "      <div id=\"color\"></div>"
			+ "   </body>"
			+ "</html>";}

@RequestMapping("/blue")
@ResponseBody
public String printBlue() {
	return "<html>"
			+ "   <head>"
			+ "      <style>"
			+ "         #color {"
			+ "            width: 1000px;"
			+ "            height: 470px;"
			+ "            background: blue;"
			+ "         }"
			+ "      </style>"
			+ "   <head>"
			+ "   <body>"
			+ "      <div id=\"color\"></div>"
			+ "   </body>"
			+ "</html>";
}
@RequestMapping("/green")
@ResponseBody
public String printGreen() {
	return "<html>"
			+ "   <head>"
			+ "      <style>"
			+ "         #color {"
			+ "            width: 1000px;"
			+ "            height: 470px;"
			+ "            background: green;"
			+ "         }"
			+ "      </style>"
			+ "   <head>"
			+ "   <body>"
			+ "      <div id=\"color\"></div>"
			+ "   </body>"
			+ "</html>";
}
}
