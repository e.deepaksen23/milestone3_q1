package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Milestone3Q1StsApplication {

	public static void main(String[] args) {
		SpringApplication.run(Milestone3Q1StsApplication.class, args);
	}

}
