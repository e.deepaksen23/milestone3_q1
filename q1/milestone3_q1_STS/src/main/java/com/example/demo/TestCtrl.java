/* this is an example of creating spring boot thorugh STS plugin
 * spring parent, spring web, devtools, jasper and maven are added while creating project
 * this example asks for favourite food and displays its image
 * */



package com.example.demo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class TestCtrl {
	@RequestMapping("/")
	public String showMenu() {
		return "/homePage";
	}
	@RequestMapping("/menu")
	public String showFood(HttpServletRequest req,HttpServletResponse res,Model model) {
		String s=null;
		String wish=req.getParameter("wish");
		if(wish.equals("biriyani")) 
		{
			s="https://img-global.cpcdn.com/recipes/c5b2de212c276cfa/680x482cq70/chicken-biriyani-with-chicken-chap-recipe-main-photo.jpg";
		}
		else if(wish.equals("icecream"))
		{
			s="https://joyfoodsunshine.com/wp-content/uploads/2020/06/homemade-chocolate-ice-cream-recipe-7.jpg";
		}
		else if(wish.equals("pizza"))
		{
			s="https://www.recipetineats.com/wp-content/uploads/2020/05/Pepperoni-Pizza_5-SQjpg.jpg";
		}
		else if(wish.equals("parota"))
		{
			s="https://farm9.staticflickr.com/8586/16023214373_90552b64f6_o.jpg";
		}
		model.addAttribute("url",s);
		model.addAttribute("wish", wish);
		return "/resultPage";
	}
}
